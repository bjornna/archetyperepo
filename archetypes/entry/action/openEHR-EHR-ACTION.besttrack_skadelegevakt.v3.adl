archetype (adl_version=1.4)
	openEHR-EHR-ACTION.besttrack_skadelegevakt.v3

concept
	[at0000]	-- Skadelegevakt
language
	original_language = <[ISO_639-1::nb]>
	translations = <
		["no"] = <
			language = <[ISO_639-1::no]>
			author = <
				["name"] = <"?">
			>
		>
	>
description
	original_author = <
		["name"] = <"Bjørn Næss">
		["organisation"] = <"DIPS ASA">
		["email"] = <"bna@dips.no">
		["date"] = <"2012-11-01">
	>
	details = <
		["no"] = <
			language = <[ISO_639-1::no]>
			purpose = <"Benyttes for å dokumentere arbeidsflyten gjennom skadelegevakten i Oslo ">
			use = <"Test">
			misuse = <"Produksjon">
		>
		["nb"] = <
			language = <[ISO_639-1::nb]>
			purpose = <"Benyttes for å dokumentere arbeidsflyten gjennom skadelegevakten i Oslo ">
			use = <"Test">
			misuse = <"Produksjon">
			copyright = <"">
		>
	>
	lifecycle_state = <"0">
	other_contributors = <>
	other_details = <
		["MD5-CAM-1.0.1"] = <"BCAFDAA1248C165C5A80374D29BE12DD">
	>

definition
	ACTION[at0000] matches {	-- Skadelegevakt
		ism_transition matches {
			ISM_TRANSITION[at0012] matches {	-- Ankomstregistrert
				current_state matches {
					DV_CODED_TEXT matches {
						defining_code matches {[openehr::526]}
					}
				}
				careflow_step matches {
					DV_CODED_TEXT matches {
						defining_code matches {[local::at0012]}		-- Ankomstregistrert
					}
				}
			}
			ISM_TRANSITION[at0014] matches {	-- Tatt inn i avdeling
				current_state matches {
					DV_CODED_TEXT matches {
						defining_code matches {[openehr::245]}
					}
				}
				careflow_step matches {
					DV_CODED_TEXT matches {
						defining_code matches {[local::at0014]}		-- Tatt inn i avdeling
					}
				}
			}
			ISM_TRANSITION[at0017] matches {	-- Tildelt legeressurs
				current_state matches {
					DV_CODED_TEXT matches {
						defining_code matches {[openehr::245]}
					}
				}
				careflow_step matches {
					DV_CODED_TEXT matches {
						defining_code matches {[local::at0017]}		-- Tildelt legeressurs
					}
				}
			}
			ISM_TRANSITION[at0030] matches {	-- Bestilt rtg
				current_state matches {
					DV_CODED_TEXT matches {
						defining_code matches {[openehr::245]}
					}
				}
				careflow_step matches {
					DV_CODED_TEXT matches {
						defining_code matches {[local::at0030]}		-- Bestilt rtg
					}
				}
			}
			ISM_TRANSITION[at0032] matches {	-- Røntgen utført 
				current_state matches {
					DV_CODED_TEXT matches {
						defining_code matches {[openehr::245]}
					}
				}
				careflow_step matches {
					DV_CODED_TEXT matches {
						defining_code matches {[local::at0032]}		-- Røntgen utført 
					}
				}
			}
			ISM_TRANSITION[at0020] matches {	-- Avslutt med kontroll
				current_state matches {
					DV_CODED_TEXT matches {
						defining_code matches {[openehr::532]}
					}
				}
				careflow_step matches {
					DV_CODED_TEXT matches {
						defining_code matches {[local::at0020]}		-- Avslutt med kontroll
					}
				}
			}
			ISM_TRANSITION[at0021] matches {	-- Avslutt uten kontroll
				current_state matches {
					DV_CODED_TEXT matches {
						defining_code matches {[openehr::532]}
					}
				}
				careflow_step matches {
					DV_CODED_TEXT matches {
						defining_code matches {[local::at0021]}		-- Avslutt uten kontroll
					}
				}
			}
		}
		description matches {
			allow_archetype ITEM_TREE matches {
				include
					archetype_id/value matches {/openEHR-EHR-ITEM_TREE\.skadelegevakt_struktur(-[a-zA-Z0-9_]+)*\.v1/}
			}
		}
	}

ontology
	term_definitions = <
		["no"] = <
			items = <
				["at0000"] = <
					text = <"Skadelegevakt">
					description = <"Aktivitet som gjennomføres ved skadelegevakten i Oslo">
				>
				["at0012"] = <
					text = <"Ankomstregistrert">
					description = <"Pasienten er ankomstregistrert og venter på behandling ">
				>
				["at0014"] = <
					text = <"Tildelt legeressurs">
					description = <"Lege dokumenterer tilsyn av pasient og har satt seg selv som ressurs">
				>
				["at0017"] = <
					text = <"Rtg. bestilt">
					description = <"Bestill røntgen ">
				>
				["at0020"] = <
					text = <"Avslutt med kontroll">
					description = <"*">
				>
				["at0021"] = <
					text = <"Avslutt uten kontroll">
					description = <"*">
				>
				["at0030"] = <
					text = <"Rtg. vurdert">
					description = <"*">
				>
				["at0032"] = <
					text = <"Tatt inn i avdeling">
					description = <"Her menes at pasienten enten får rom, forheng, el på annet vis har blitt sett på i avdeling og behandlingen startet">
				>
			>
		>
		["nb"] = <
			items = <
				["at0000"] = <
					text = <"Skadelegevakt">
					description = <"Aktivitet som gjennomføres ved skadelegevakten i Oslo">
				>
				["at0012"] = <
					text = <"Ankomstregistrert">
					description = <"Pasienten er ankomstregistrert og venter på behandling ">
				>
				["at0014"] = <
					text = <"Tatt inn i avdeling">
					description = <"Pasient er tatt inn på avdeling av sykepleier eller lege">
				>
				["at0017"] = <
					text = <"Tildelt legeressurs">
					description = <"Pasienten har fått tildelt en legeressurs - legen har angitt sine initialer ">
				>
				["at0020"] = <
					text = <"Avslutt med kontroll">
					description = <"*">
				>
				["at0021"] = <
					text = <"Avslutt uten kontroll">
					description = <"*">
				>
				["at0030"] = <
					text = <"Bestilt rtg">
					description = <"Røntgen er bestilt på pasienten - pasient venter på å få utført røntgen">
				>
				["at0032"] = <
					text = <"Røntgen utført ">
					description = <"Røntgen har tatt bilde. Pasient er ferdig på røntgen. Venter på at lege skal vurdere bildet">
				>
			>
		>
	>
