﻿archetype (adl_version=1.4)
	openEHR-EHR-EVALUATION.alcohol_use_summary.v1

concept
	[at0000]	-- Sammendrag av alkoholbruk
language
	original_language = <[ISO_639-1::en]>
	translations = <
		["no"] = <
			language = <[ISO_639-1::no]>
			author = <
				["name"] = <"?">
			>
		>
	>
description
	original_author = <
		["name"] = <"Heather Leslie">
		["organisation"] = <"Ocean Informatics">
		["email"] = <"heather.leslie@oceaninformatics.com">
		["date"] = <"2011-07-15">
	>
	details = <
		["no"] = <
			language = <[ISO_639-1::no]>
			purpose = <"To record summary and persisting information about the consumption of alcohol.(en)">
			use = <"Use to record summary and persisting information about the consumption of alcohol.

Data that might be used to assess the risk of alcohol abuse or dependence will be recorded using two archetypes: this EVALUATION archetype (recording the summary and persisting data) and in the OBSERVATION.alcohol_use archetype (recording the repeatable observations/measurements).(en)">
			keywords = <"alcohol(en)", "beer(en)", "wine(en)", "spirits(en)", "fortified(en)", "consumption(en)", "use(en)", "status(en)", "abuse(en)", "binge(en)">
			misuse = <"Not to be used to record event-or period-based information about alcohol use or consumption, such as actual daily use or the average use over a specified period of time - use the OBSERVATION.alcohol_use archetype.

Not to be used to record information about consumption of other substances other than alcohol.(en)">
			copyright = <"© openEHR Foundation(en)">
		>
		["en"] = <
			language = <[ISO_639-1::en]>
			purpose = <"To record summary and persisting information about the consumption of alcohol.">
			use = <"Use to record summary and persisting information about the consumption of alcohol.

Data that might be used to assess the risk of alcohol abuse or dependence will be recorded using two archetypes: this EVALUATION archetype (recording the summary and persisting data) and in the OBSERVATION.alcohol_use archetype (recording the repeatable observations/measurements).">
			keywords = <"alcohol", "beer", "wine", "spirits", "fortified", "consumption", "use", "status", "abuse", "binge">
			misuse = <"Not to be used to record event-or period-based information about alcohol use or consumption, such as actual daily use or the average use over a specified period of time - use the OBSERVATION.alcohol_use archetype.

Not to be used to record information about consumption of other substances other than alcohol.">
			copyright = <"© openEHR Foundation">
		>
	>
	lifecycle_state = <"CommitteeDraft">
	other_contributors = <"Rita Apelt, Department of Health,NT, Australia", "Stephen Chu, NEHTA, Australia", "Matthew Cordell, NEHTA, Australia", "David Evans, Queensland Health, Australia", "Tim Garden, NTG Department of Health, Australia", "Andrew Goodchild, NEHTA, Australia", "Sam Heard, Ocean Informatics, Australia (Editor)", "Mary Kelaher, NEHTA, Australia", "Robert L'Egan, NEHTA, Australia", "Heather Leslie, Ocean Informatics, Australia (Editor)", "Ian McNicoll, Ocean Informatics UK, United Kingdom", "Chris Mitchell, RACGP, Australia", "Stewart Morrison, NEHTA, Australia", "Jeremy Oats, NT Health, Australia", "Rosalie Schultz, Central Australia Remote Health, Australia", "John Taylor, NEHTA, Australia", "Richard Townley-O'Neill, NEHTA, Australia", "Jo Wright, NT Dept of Health, Australia (Editor)">
	other_details = <
		["references"] = <"openEHR Clinical Knowledge Manager [Internet]. London: openEHR Foundation. [Draft EVALUATION archetype] Alcohol Use Summary; [authored 2009 Jun 21, cited 2011 Jul 15]. Available from: http://www.openehr.org/knowledge/OKM.html#showArchetype_1013.1.362_1.

NEHTA Clinical Knowledge Manager [Internet]. : NEHTA. [Draft EVALUATION archetype] Alcohol Use Summary; [authored 2009 Jun 21, cited 2013 Nov 02].
Available from: http://dcm.nehta.org.au/ckm/#showArchetype_1013.1.1028_3">
		["current_contact"] = <"Heather Leslie, Ocean Informatics, heather.leslie@oceaninformatics.com">
		["MD5-CAM-1.0.1"] = <"79C3953BF7DF237EE8ECD40CD5A1CFC7">
	>

definition
	EVALUATION[at0000] matches {	-- Sammendrag av alkoholbruk
		data matches {
			ITEM_TREE[at0001] matches {	-- *Tree(en)
				items cardinality matches {0..*; unordered} matches {
					ELEMENT[at0002] occurrences matches {0..1} matches {	-- Alkoholbruk
						value matches {
							DV_CODED_TEXT matches {
								defining_code matches {
									[local::
									at0003, 	-- Bruker alkohol
									at0004, 	-- Nåværende avhold, tidligere bruker av alkohol
									at0006]	-- Alltid vært avholder
								}
							}
						}
					}
					ELEMENT[at0024] occurrences matches {0..1} matches {	-- Typisk alkoholforbruk (antall enheter/tid)
						value matches {
							C_DV_QUANTITY <
								property = <[openehr::382]>
								list = <
									["1"] = <
										units = <"/d">
										magnitude = <|>=0.0|>
										precision = <|0|>
									>
									["2"] = <
										units = <"/wk">
									>
								>
							>
							DV_INTERVAL<DV_QUANTITY> matches {
								upper matches {
									C_DV_QUANTITY <
										property = <[openehr::382]>
										list = <
											["1"] = <
												units = <"/d">
												magnitude = <|>=0.0|>
												precision = <|0|>
											>
											["2"] = <
												units = <"/wk">
											>
											["3"] = <
												units = <"/mo">
											>
										>
									>
								}
								lower matches {
									C_DV_QUANTITY <
										property = <[openehr::382]>
										list = <
											["1"] = <
												units = <"/d">
												magnitude = <|>=0.0|>
												precision = <|0|>
											>
											["2"] = <
												units = <"/wk">
											>
											["3"] = <
												units = <"/mo">
											>
										>
									>
								}
							}
							DV_TEXT matches {*}
						}
					}
					CLUSTER[at0034] occurrences matches {0..1} matches {	-- Detaljert beskrivelse av alkoholbruk
						items cardinality matches {1..*; unordered} matches {
							ELEMENT[at0025] occurrences matches {0..*} matches {	-- Type alkohol
								value matches {
									DV_TEXT matches {*}
								}
							}
							ELEMENT[at0027] occurrences matches {0..1} matches {	-- Bruksmønster
								value matches {
									DV_TEXT matches {*}
								}
							}
							ELEMENT[at0026] occurrences matches {0..1} matches {	-- Overstadig alkoholbruk
								value matches {
									DV_CODED_TEXT matches {
										defining_code matches {
											[local::
											at0028, 	-- Aldri
											at0029, 	-- Sjeldnere enn en gang i måneden
											at0030, 	-- Månedlig
											at0031, 	-- Ukentlig
											at0032]	-- Daglig
										}
									}
								}
							}
							ELEMENT[at0015] occurrences matches {0..1} matches {	-- Første konsumentdato
								value matches {
									DV_DATE_TIME matches {*}
								}
							}
							ELEMENT[at0016] occurrences matches {0..1} matches {	-- Siste konsumentdato før slutt
								value matches {
									DV_DATE_TIME matches {*}
								}
							}
							allow_archetype CLUSTER[at0023] occurrences matches {0..*} matches {	-- Sluttforsøk
								include
									archetype_id/value matches {/openEHR-EHR-CLUSTER\.cessation_attempts(-[a-zA-Z0-9_]+)*\.v1/}
							}
							ELEMENT[at0019] occurrences matches {0..1} matches {	-- Kommentarer
								value matches {
									DV_TEXT matches {*}
								}
							}
						}
					}
				}
			}
		}
		protocol matches {
			ITEM_TREE[at0021] matches {	-- *Tree(en)
				items cardinality matches {0..*; unordered} matches {
					ELEMENT[at0022] occurrences matches {0..1} matches {	-- Dato for siste oppdatering
						value matches {
							DV_DATE_TIME matches {*}
						}
					}
					ELEMENT[at0033] occurrences matches {0..1} matches {	-- Antall gram alkohol i en typisk bruksenhet
						value matches {
							C_DV_QUANTITY <
								property = <[openehr::124]>
								list = <
									["1"] = <
										units = <"gm">
										magnitude = <|>=0.0|>
									>
								>
							>
						}
					}
				}
			}
		}
	}

ontology
	term_definitions = <
		["no"] = <
			items = <
				["at0000"] = <
					text = <"Sammendrag av alkoholbruk">
					description = <"Summary or persisting information about alcohol use or consumption.(en)">
				>
				["at0001"] = <
					text = <"Tree(en)">
					description = <"@ internal @(en)">
				>
				["at0002"] = <
					text = <"Alkoholbruk">
					description = <"Statement about the individual's overall pattern of usage or consumption of alcohol.(en)">
				>
				["at0003"] = <
					text = <"Bruker alkohol">
					description = <"Individual is a current consumer of alcohol.(en)">
				>
				["at0004"] = <
					text = <"Nåværende avhold, tidligere bruker av alkohol">
					description = <"Individual is a former or ex-consumer of alcohol.(en)">
				>
				["at0006"] = <
					text = <"Alltid vært avholder">
					description = <"Individual has never consumed alcohol.(en)">
				>
				["at0015"] = <
					description = <"Date that any consumption of alcohol commenced.(en)">
					text = <"Første konsumentdato">
					comment = <"In most situations it is likely that only a partial date will be recorded, for example, only the year of commencement.(en)">
				>
				["at0016"] = <
					description = <"Date that all consumption of alcohol ceased.(en)">
					text = <"Siste konsumentdato før slutt">
					comment = <"Can be a partial date, for example, only a year.(en)">
				>
				["at0019"] = <
					text = <"Kommentarer">
					description = <"Additional narrative about the alcohol use or consumption pattern not captured in other fields.(en)">
				>
				["at0021"] = <
					text = <"Tree(en)">
					description = <"@ internal @(en)">
				>
				["at0022"] = <
					text = <"Dato for siste oppdatering">
					description = <"The date this alcohol consumption summary was last updated.(en)">
				>
				["at0023"] = <
					text = <"Sluttforsøk">
					description = <"Details about attempts to cease use of alcohol.(en)">
				>
				["at0024"] = <
					description = <"Estimate of typical alcohol consumption, in number of standard drinks/units per day - either as a whole number, a range, or as a term, normally coded. Definitions of standard units/drinks vary considerably.(en)">
					text = <"Typisk alkoholforbruk (antall enheter/tid)">
					comment = <"This data element allows a rough indication of alcohol consumption to be recorded, for example 5-10 per day. The period of time is not specified. If exact consumption at specific points in time or averages/maximums over specified intervals of time are required, use the OBSERVATION.alcohol_use archetype.(en)">
				>
				["at0025"] = <
					description = <"The form or type of alcohol consumed.(en)">
					text = <"Type alkohol">
					comment = <">> 53527002 | alcoholic beverage | (en)">
				>
				["at0026"] = <
					text = <"Overstadig alkoholbruk">
					description = <"The subject's pattern of heavy, episodic drinking.(en)">
				>
				["at0027"] = <
					text = <"Bruksmønster">
					description = <"The typical pattern of the subject's use of alcohol.(en)">
				>
				["at0028"] = <
					text = <"Aldri">
					description = <"The subject does not binge drink.(en)">
				>
				["at0029"] = <
					text = <"Sjeldnere enn en gang i måneden">
					description = <"The subject binge drinks less than once per month.(en)">
				>
				["at0030"] = <
					text = <"Månedlig">
					description = <"The subject binge drinks on a monthly basis.(en)">
				>
				["at0031"] = <
					text = <"Ukentlig">
					description = <"The subject binge drinks on a weekly basis.(en)">
				>
				["at0032"] = <
					text = <"Daglig">
					description = <"The subject binge drinks on a daily or almost daily basis.(en)">
				>
				["at0033"] = <
					text = <"Antall gram alkohol i en typisk bruksenhet">
					description = <"Amount of alcohol defining a standard drink or unit.(en)">
				>
				["at0034"] = <
					text = <"Detaljert beskrivelse av alkoholbruk">
					description = <"">
				>
			>
		>
		["en"] = <
			items = <
				["at0000"] = <
					text = <"Alcohol Use Summary">
					description = <"Summary or persisting information about alcohol use or consumption.">
				>
				["at0001"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0002"] = <
					text = <"Status">
					description = <"Statement about the individual's overall pattern of usage or consumption of alcohol.">
				>
				["at0003"] = <
					text = <"Current drinker">
					description = <"Individual is a current consumer of alcohol.">
				>
				["at0004"] = <
					text = <"Ex-drinker">
					description = <"Individual is a former or ex-consumer of alcohol.">
				>
				["at0006"] = <
					text = <"Lifetime non-drinker">
					description = <"Individual has never consumed alcohol.">
				>
				["at0015"] = <
					description = <"Date that any consumption of alcohol commenced.">
					text = <"Date Commenced">
					comment = <"In most situations it is likely that only a partial date will be recorded, for example, only the year of commencement.">
				>
				["at0016"] = <
					description = <"Date that all consumption of alcohol ceased.">
					text = <"Date Ceased">
					comment = <"Can be a partial date, for example, only a year.">
				>
				["at0019"] = <
					text = <"Comment">
					description = <"Additional narrative about the alcohol use or consumption pattern not captured in other fields.">
				>
				["at0021"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0022"] = <
					text = <"Date Last Updated">
					description = <"The date this alcohol consumption summary was last updated.">
				>
				["at0023"] = <
					text = <"Cessation Attempts">
					description = <"Details about attempts to cease use of alcohol.">
				>
				["at0024"] = <
					description = <"Estimate of typical alcohol consumption, in number of standard drinks/units per day - either as a whole number, a range, or as a term, normally coded. Definitions of standard units/drinks vary considerably.">
					text = <"Typical Alcohol Consumption">
					comment = <"This data element allows a rough indication of alcohol consumption to be recorded, for example 5-10 per day. The period of time is not specified. If exact consumption at specific points in time or averages/maximums over specified intervals of time are required, use the OBSERVATION.alcohol_use archetype.">
				>
				["at0025"] = <
					description = <"The form or type of alcohol consumed.">
					text = <"Form">
					comment = <">> 53527002 | alcoholic beverage | ">
				>
				["at0026"] = <
					text = <"Binge Drinking Pattern">
					description = <"The subject's pattern of heavy, episodic drinking.">
				>
				["at0027"] = <
					text = <"Pattern of Use">
					description = <"The typical pattern of the subject's use of alcohol.">
				>
				["at0028"] = <
					text = <"None">
					description = <"The subject does not binge drink.">
				>
				["at0029"] = <
					text = <"Less than once per month">
					description = <"The subject binge drinks less than once per month.">
				>
				["at0030"] = <
					text = <"Monthly">
					description = <"The subject binge drinks on a monthly basis.">
				>
				["at0031"] = <
					text = <"Weekly">
					description = <"The subject binge drinks on a weekly basis.">
				>
				["at0032"] = <
					text = <"Daily">
					description = <"The subject binge drinks on a daily or almost daily basis.">
				>
				["at0033"] = <
					text = <"Standard Drink Definition">
					description = <"Amount of alcohol defining a standard drink or unit.">
				>
				["at0034"] = <
					text = <"New cluster(en)">
					description = <"(en)">
				>
			>
		>
	>
