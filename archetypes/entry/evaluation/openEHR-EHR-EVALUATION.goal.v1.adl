archetype (adl_version=1.4)
	openEHR-EHR-EVALUATION.goal.v1

concept
	[at0000]	-- Mål
language
	original_language = <[ISO_639-1::en]>
	translations = <
		["no"] = <
			language = <[ISO_639-1::no]>
			author = <
				["name"] = <"?">
			>
		>
		["nb"] = <
			language = <[ISO_639-1::nb]>
			author = <
				["name"] = <"?">
			>
		>
	>
description
	original_author = <
		["name"] = <"Sam Heard">
		["organisation"] = <"Ocean Informatics">
		["email"] = <"sam.heard@oceaninformatics.com">
		["date"] = <"23/04/2006">
	>
	details = <
		["no"] = <
			language = <[ISO_639-1::no]>
			purpose = <"For opptak generelle mål og konkrete målbare (og kanskje inkrementell) mål mot det målet">
			use = <"">
			keywords = <"mål", ...>
			misuse = <"">
			copyright = <"© openEHR Foundation">
		>
		["en"] = <
			language = <[ISO_639-1::en]>
			purpose = <"For recording general goals and specific measurable (and perhaps incremental) targets towards that goal">
			use = <"">
			keywords = <"target", ...>
			misuse = <"">
			copyright = <"© openEHR Foundation">
		>
		["nb"] = <
			language = <[ISO_639-1::nb]>
			purpose = <"For opptak generelle mål og konkrete målbare (og kanskje inkrementell) mål mot det målet">
			use = <"">
			keywords = <"mål", ...>
			misuse = <"">
			copyright = <"© openEHR Foundation">
		>
	>
	lifecycle_state = <"AuthorDraft">
	other_contributors = <>
	other_details = <
		["MD5-CAM-1.0.1"] = <"0F414E695A8DF734DEC220BB543F64BC">
	>

definition
	EVALUATION[at0000] matches {	-- Mål
		data matches {
			ITEM_TREE[at0001] matches {	-- *Tree(en)
				items cardinality matches {1..*; ordered} matches {
					ELEMENT[at0002] matches {	-- Resultat
						value matches {
							DV_TEXT matches {*}
						}
					}
					ELEMENT[at0003] occurrences matches {0..1} matches {	-- Foreslått dato for oppnåelse
						value matches {
							DV_DATE matches {*}
						}
					}
					ELEMENT[at0004] occurrences matches {0..1} matches {	-- Faktisk dato for oppnåelse
						value matches {
							DV_DATE matches {*}
						}
					}
					CLUSTER[at0005] occurrences matches {0..*} matches {	-- Mål
						items cardinality matches {1..4; ordered} matches {
							ELEMENT[at0006] matches {	-- Variabel
								value matches {
									DV_TEXT matches {*}
								}
							}
							ELEMENT[at0007] occurrences matches {0..1} matches {	-- Oppnåelse av mål
								value matches {
									DV_TEXT matches {*}
								}
							}
							ELEMENT[at0008] occurrences matches {0..1} matches {	-- Foreslått måldato
								value matches {
									DV_DATE matches {*}
								}
							}
							ELEMENT[at0009] occurrences matches {0..1} matches {	-- Faktisk måldato
								value matches {
									DV_DATE matches {*}
								}
							}
						}
					}
				}
			}
		}
	}

ontology
	term_definitions = <
		["no"] = <
			items = <
				["at0000"] = <
					text = <"Mål">
					description = <"En fremtidig helsetilstand som er avtalt med personen ">
				>
				["at0001"] = <
					text = <"*Tree(en)">
					description = <"*@ internal @(en)">
				>
				["at0002"] = <
					text = <"Resultat">
					description = <"*The health state that is to be achieved(en)">
				>
				["at0003"] = <
					text = <"Foreslått dato for oppnåelse">
					description = <"*The proposed date of achievement(en)">
				>
				["at0004"] = <
					text = <"Faktisk dato for oppnåelse">
					description = <"*The date the outcome was achieved(en)">
				>
				["at0005"] = <
					text = <"Mål">
					description = <"*The target outcome(en)">
				>
				["at0006"] = <
					text = <"Variabel">
					description = <"Arketype og sti til noden som skal måles ">
				>
				["at0007"] = <
					text = <"Oppnåelse av mål">
					description = <"Verdiområder for målet">
				>
				["at0008"] = <
					text = <"Foreslått måldato">
					description = <"Foreslått dato for å oppnå målet ">
				>
				["at0009"] = <
					text = <"Faktisk måldato">
					description = <"Den faktiske dato hvor målet ble nådd">
				>
			>
		>
		["nb"] = <
			items = <
				["at0000"] = <
					text = <"Mål">
					description = <"En fremtidig helsetilstand som er avtalt med personen ">
				>
				["at0001"] = <
					text = <"*Tree(en)">
					description = <"*@ internal @(en)">
				>
				["at0002"] = <
					text = <"Resultat">
					description = <"*The health state that is to be achieved(en)">
				>
				["at0003"] = <
					text = <"Foreslått dato for oppnåelse">
					description = <"*The proposed date of achievement(en)">
				>
				["at0004"] = <
					text = <"Faktisk dato for oppnåelse">
					description = <"*The date the outcome was achieved(en)">
				>
				["at0005"] = <
					text = <"Mål">
					description = <"*The target outcome(en)">
				>
				["at0006"] = <
					text = <"Variabel">
					description = <"Arketype og sti til noden som skal måles ">
				>
				["at0007"] = <
					text = <"Oppnåelse av mål">
					description = <"Verdiområder for målet">
				>
				["at0008"] = <
					text = <"Foreslått måldato">
					description = <"Foreslått dato for å oppnå målet ">
				>
				["at0009"] = <
					text = <"Faktisk måldato">
					description = <"Den faktiske dato hvor målet ble nådd">
				>
			>
		>
		["en"] = <
			items = <
				["at0000"] = <
					text = <"Goal">
					description = <"A future health state that is agreed to by the person">
				>
				["at0001"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0002"] = <
					text = <"Outcome">
					description = <"The health state that is to be achieved">
				>
				["at0003"] = <
					text = <"Proposed date of achievement">
					description = <"The proposed date of achievement">
				>
				["at0004"] = <
					text = <"Actual date of achievement">
					description = <"The date the outcome was achieved">
				>
				["at0005"] = <
					text = <"Target">
					description = <"The target outcome">
				>
				["at0006"] = <
					text = <"Archetype node path">
					description = <"The archetype and path to the node that is to be measured">
				>
				["at0007"] = <
					text = <"Target measurement">
					description = <"The range of values within the target">
				>
				["at0008"] = <
					text = <"Proposed target date">
					description = <"The proposed date for achieving the target">
				>
				["at0009"] = <
					text = <"Actual target date">
					description = <"The actual date the target was achieved">
				>
			>
		>
	>
