archetype (adl_version=1.4)
	openEHR-EHR-EVALUATION.prev_diseases.v1

concept
	[at0000]	-- Tidligere sykdommer
language
	original_language = <[ISO_639-1::nb]>
	translations = <
		["no"] = <
			language = <[ISO_639-1::no]>
			author = <
				["name"] = <"?">
			>
		>
	>
description
	original_author = <
		["name"] = <"">
	>
	details = <
		["no"] = <
			language = <[ISO_639-1::no]>
			purpose = <"For å registrere tidligere sykdommer på en pasient.">
			use = <"">
			misuse = <"">
			copyright = <"">
		>
		["nb"] = <
			language = <[ISO_639-1::nb]>
			purpose = <"For å registrere tidligere sykdommer på en pasient.">
			use = <"">
			misuse = <"">
			copyright = <"">
		>
	>
	lifecycle_state = <"0">
	other_contributors = <>
	other_details = <
		["MD5-CAM-1.0.1"] = <"27FAD441A9442394435AA1C7898EC596">
	>

definition
	EVALUATION[at0000] matches {	-- Tidligere sykdommer
		data matches {
			ITEM_TREE[at0001] matches {	-- Tree
				items cardinality matches {0..*; unordered} matches {
					CLUSTER[at0002] occurrences matches {0..*} matches {	-- Sykdom/tilstand
						items cardinality matches {2..*; unordered} matches {
							ELEMENT[at0003] occurrences matches {0..1} matches {	-- Tid
								value matches {
									DV_COUNT matches {
										magnitude matches {|>=1000|}
									}
								}
							}
							ELEMENT[at0004] occurrences matches {0..1} matches {	-- Beskrivelse
								value matches {
									DV_TEXT matches {*}
								}
							}
							CLUSTER[at0005] occurrences matches {0..*} matches {	-- Knytt til diagnosekode
								items cardinality matches {1..*; unordered} matches {
									ELEMENT[at0007] occurrences matches {0..1} matches {	-- Kode
										value matches {
											DV_TEXT matches {*}
										}
									}
									ELEMENT[at0008] occurrences matches {0..1} matches {	-- Kommentar
										value matches {
											DV_TEXT matches {*}
										}
									}
								}
							}
							CLUSTER[at0006] occurrences matches {0..*} matches {	-- Knytt til prosedyrekode
								items cardinality matches {1..*; unordered} matches {
									ELEMENT[at0009] occurrences matches {0..1} matches {	-- Kode
										value matches {
											DV_TEXT matches {*}
										}
									}
									ELEMENT[at0010] occurrences matches {0..1} matches {	-- Kommentar
										value matches {
											DV_TEXT matches {*}
										}
									}
								}
							}
						}
					}
					CLUSTER[at0011] occurrences matches {0..1} matches {	-- Tidligere frisk
						items cardinality matches {1..*; unordered} matches {
							ELEMENT[at0015] occurrences matches {0..1} matches {	-- Beskrivelse
								value matches {
									DV_TEXT matches {*}
								}
							}
						}
					}
					CLUSTER[at0013] occurrences matches {0..1} matches {	-- Informasjon mangler
						items cardinality matches {1..*; unordered} matches {
							ELEMENT[at0014] occurrences matches {0..1} matches {	-- Beskrivelse
								value matches {
									DV_TEXT matches {*}
								}
							}
						}
					}
				}
			}
		}
	}

ontology
	term_definitions = <
		["no"] = <
			items = <
				["at0000"] = <
					text = <"Tidligere sykdommer">
					description = <"Registrerer tidligere sykdommer på en pasient. ">
				>
				["at0001"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0002"] = <
					text = <"Sykdom/tilstand">
					description = <"Sykdom eller tilstand ">
				>
				["at0003"] = <
					text = <"Tid">
					description = <"*">
				>
				["at0004"] = <
					text = <"Beskrivelse">
					description = <"*">
				>
				["at0005"] = <
					text = <"Knytt til diagnosekode">
					description = <"*">
				>
				["at0006"] = <
					text = <"Knytt til prosedyrekode">
					description = <"*">
				>
				["at0007"] = <
					text = <"Kode">
					description = <"*">
				>
				["at0008"] = <
					text = <"Kommentar">
					description = <"*">
				>
				["at0009"] = <
					text = <"Kode">
					description = <"*">
				>
				["at0010"] = <
					text = <"Kommentar">
					description = <"*">
				>
				["at0011"] = <
					text = <"Tidligere frisk">
					description = <"*">
				>
				["at0013"] = <
					text = <"Informasjon mangler">
					description = <"">
				>
				["at0014"] = <
					text = <"Beskrivelse">
					description = <"Default tekst: informasjon mangler ">
				>
				["at0015"] = <
					text = <"Beskrivelse">
					description = <"Tidligere frisk">
				>
			>
		>
		["nb"] = <
			items = <
				["at0000"] = <
					text = <"Tidligere sykdommer">
					description = <"Registrerer tidligere sykdommer på en pasient. ">
				>
				["at0001"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0002"] = <
					text = <"Sykdom/tilstand">
					description = <"Sykdom">
				>
				["at0003"] = <
					text = <"Tid">
					description = <"*">
				>
				["at0004"] = <
					text = <"Beskrivelse">
					description = <"*">
				>
				["at0005"] = <
					text = <"Problem/diagnose">
					description = <"*">
				>
				["at0006"] = <
					text = <"Prosedyre">
					description = <"*">
				>
				["at0007"] = <
					text = <"Kode">
					description = <"*">
				>
				["at0008"] = <
					text = <"Kommentar">
					description = <"*">
				>
				["at0009"] = <
					text = <"Kode">
					description = <"*">
				>
				["at0010"] = <
					text = <"Kommentar">
					description = <"*">
				>
				["at0011"] = <
					text = <"Tidligere frisk">
					description = <"*">
				>
				["at0013"] = <
					text = <"Informasjon mangler">
					description = <"*">
				>
				["at0014"] = <
					text = <"Beskrivelse">
					description = <"Default tekst: informasjon mangler ">
				>
				["at0015"] = <
					text = <"Beskrivelse">
					description = <"Tidligere frisk">
				>
			>
		>
	>
