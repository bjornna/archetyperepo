﻿archetype (adl_version=1.4)
	openEHR-EHR-INSTRUCTION.request-referral.v1
specialize
	openEHR-EHR-INSTRUCTION.request.v1

concept
	[at0000.1]	-- Henvisning
language
	original_language = <[ISO_639-1::en]>
	translations = <
		["no"] = <
			language = <[ISO_639-1::no]>
			author = <
				["name"] = <"?">
			>
		>
	>
description
	original_author = <
		["name"] = <"Dr Ian McNicoll">
		["organisation"] = <"Ocean Informatics, United Kingdom">
		["email"] = <"ian.mcnicoll@oceaninformatics.com">
		["date"] = <"08/12/2009">
	>
	details = <
		["no"] = <
			language = <[ISO_639-1::no]>
			purpose = <"*To communicate details of a referral request to another healthcare provider or organisation for provision of a specified service.(en)">
			use = <"*Use to communicate the details of the request for provision of a specified service by another healthcare provider or organisation.  This request will usually be supported by additional information that provide clinical context about the patient using other archetypes.(en)">
			keywords = <"*request(en)", "*refer(en)", "*order(en)", "*service(en)", "*provide(en)">
			misuse = <"*(en)">
			copyright = <"*© openEHR Foundation(en)">
		>
		["en"] = <
			language = <[ISO_639-1::en]>
			purpose = <"To communicate details of a referral request to another healthcare provider or organisation for provision of a specified service.">
			use = <"Use to communicate the details of the request for provision of a specified service by another healthcare provider or organisation.  This request will usually be supported by additional information that provide clinical context about the patient using other archetypes.">
			keywords = <"request", "refer", "order", "service", "provide">
			misuse = <"">
			copyright = <"© openEHR Foundation">
		>
	>
	lifecycle_state = <"AuthorDraft">
	other_contributors = <"Heather Leslie, Ocean Informatics, Australia", ...>
	other_details = <
		["MD5-CAM-1.0.1"] = <"A51A505096BC930373C39B77EE3E73AB">
	>

definition
	INSTRUCTION[at0000.1] matches {	-- Henvisning
		activities cardinality matches {0..*; unordered} matches {
			ACTIVITY[at0001] occurrences matches {1..*} matches {	-- *Request(en)
				description matches {
					ITEM_TREE[at0009] matches {	-- *Tree(en)
						items cardinality matches {1..*; unordered} matches {
							ELEMENT[at0121] matches {	-- *Service requested(en)
								value matches {
									DV_TEXT matches {*}
								}
							}
							ELEMENT[at0135] occurrences matches {0..1} matches {	-- *Description of service(en)
								value matches {
									DV_TEXT matches {*}
								}
							}
							allow_archetype CLUSTER[at0132] occurrences matches {0..*} matches {	-- *Specific details(en)
								include
									archetype_id/value matches {/.*/}
							}
							ELEMENT[at0062] occurrences matches {0..1} matches {	-- *Reason for request(en)
								value matches {
									DV_TEXT matches {*}
								}
							}
							ELEMENT[at0064] occurrences matches {0..1} matches {	-- *Reason description(en)
								value matches {
									DV_TEXT matches {*}
								}
							}
							ELEMENT[at0065] occurrences matches {0..1} matches {	-- *Intent(en)
								value matches {
									DV_TEXT matches {*}
								}
							}
							ELEMENT[at0068] occurrences matches {0..1} matches {	-- *Urgency(en)
								value matches {
									DV_CODED_TEXT matches {
										defining_code matches {
											[local::
											at0136, 	-- *Emergency(en)
											at0137, 	-- *Urgent(en)
											at0138]	-- *Routine(en)
										}
									}
								}
							}
							ELEMENT[at0040] occurrences matches {0..1} matches {	-- *Date &/or time service required(en)
								value matches {
									DV_DATE_TIME matches {*}
								}
							}
							ELEMENT[at0144] occurrences matches {0..1} matches {	-- *Latest date service required(en)
								value matches {
									DV_DATE_TIME matches {*}
								}
							}
							ELEMENT[at0076] occurrences matches {0..1} matches {	-- *Supplementary information to follow(en)
								value matches {
									DV_BOOLEAN matches {
										value matches {True}
									}
								}
							}
							ELEMENT[at0078] occurrences matches {0..1} matches {	-- *Supplementary information expected(en)
								value matches {
									DV_TEXT matches {*}
								}
							}
							allow_archetype CLUSTER[at0116] occurrences matches {0..*} matches {	-- *Patient requirements(en)
								include
									archetype_id/value matches {/.*/}
							}
						}
					}
				}
			}
		}
		protocol matches {
			ITEM_TREE[at0008] matches {	-- *Tree(en)
				items cardinality matches {1..*; unordered} matches {
					ELEMENT[at0010] occurrences matches {0..1} matches {	-- *Requestor Identifier(en)
						value matches {
							DV_TEXT matches {*}
						}
					}
					allow_archetype CLUSTER[at0141] occurrences matches {0..*} matches {	-- *Requestor(en)
						include
							archetype_id/value matches {/.*/}
					}
					ELEMENT[at0011] occurrences matches {0..1} matches {	-- *Receiver identifier(en)
						value matches {
							DV_TEXT matches {*}
						}
					}
					allow_archetype CLUSTER[at0142] occurrences matches {0..*} matches {	-- *Receiver(en)
						include
							archetype_id/value matches {/.*/}
					}
					ELEMENT[at0127] occurrences matches {0..1} matches {	-- *Request status(en)
						value matches {
							DV_TEXT matches {*}
						}
					}
					allow_archetype CLUSTER[at0128] occurrences matches {0..*} matches {	-- *Distribution list for response(en)
						include
							archetype_id/value matches {/openEHR-EHR-CLUSTER\.distribution\.v1/}
					}
					allow_archetype CLUSTER[at0.5] occurrences matches {0..*} matches {	-- *Contacts(en)
						include
							archetype_id/value matches {/.*/}
					}
					CLUSTER[at0.2] occurrences matches {0..1} matches {	-- *Duration(en)
						items cardinality matches {1..*; unordered} matches {
							ELEMENT[at0.3] occurrences matches {0..1} matches {	-- *Duration(en)
								value matches {
									DV_DURATION matches {*}
								}
							}
							ELEMENT[at0.4] occurrences matches {0..1} matches {	-- *Indefinite(en)
								value matches {
									DV_BOOLEAN matches {
										value matches {True}
									}
								}
							}
						}
					}
					allow_archetype CLUSTER[at0112] occurrences matches {0..*} matches {	-- *Localisation(en)
						include
							archetype_id/value matches {/.*/}
					}
				}
			}
		}
	}

ontology
	term_definitions = <
		["no"] = <
			items = <
				["at0.2"] = <
					text = <"*Duration(en)">
					description = <"*Length of time the referral is valid.(en)">
				>
				["at0.3"] = <
					text = <"*Duration(en)">
					description = <"*Duration for which the referral is valid.(en)">
				>
				["at0.4"] = <
					text = <"*Indefinite(en)">
					description = <"*If true, referral is for an indefinite period of time.(en)">
				>
				["at0.5"] = <
					text = <"*Contacts(en)">
					description = <"*A list of people or organisations who are relevant to this referral, including next of kin.(en)">
				>
				["at0000"] = <
					text = <"*Healthcare service request(en)">
					description = <"*Generic request for a range of different healthcare services e.g referral, lab request, equipment request.(en)">
				>
				["at0000.1"] = <
					text = <"Henvisning">
					description = <"*Request for provision of a specified service by another healthcare provider or organisation.(en)">
				>
				["at0001"] = <
					text = <"*Request(en)">
					description = <"*Current Activity.(en)">
				>
				["at0008"] = <
					text = <"*Tree(en)">
					description = <"*@ internal @(en)">
				>
				["at0009"] = <
					text = <"*Tree(en)">
					description = <"*@ internal @(en)">
				>
				["at0010"] = <
					text = <"*Requestor Identifier(en)">
					description = <"*The local ID assigned to the order by the healthcare provider or organisation requesting the service. This is also referred to as Placer Order Identifier.(en)">
				>
				["at0011"] = <
					text = <"*Receiver identifier(en)">
					description = <"*The ID assigned to the order by the healthcare provider or organisation receiving the request for service. This is also referred to as Filler Order Identifier.(en)">
				>
				["at0040"] = <
					text = <"*Date &/or time service required(en)">
					description = <"*The date and time that the service should be performed or completed.(en)">
				>
				["at0062"] = <
					text = <"*Reason for request(en)">
					description = <"*A short description of the reason for the request.  This is often coded with an external terminology.(en)">
				>
				["at0064"] = <
					text = <"*Reason description(en)">
					description = <"*A narrative description explaining the reason for request.(en)">
				>
				["at0065"] = <
					text = <"*Intent(en)">
					description = <"*Stated intent of the request by the referrer.(en)">
				>
				["at0068"] = <
					text = <"*Urgency(en)">
					description = <"*Urgency of the request.(en)">
				>
				["at0076"] = <
					text = <"*Supplementary information to follow(en)">
					description = <"*True indicates that additional information has been identified and will be forwarded when available eg incomplete pathology test results.(en)">
				>
				["at0078"] = <
					text = <"*Supplementary information expected(en)">
					description = <"*Details of the nature of supplementary information that is to follow e.g name of laboratory results.(en)">
				>
				["at0112"] = <
					text = <"*Localisation(en)">
					description = <"*Local detailed information such as billing requirements.(en)">
				>
				["at0116"] = <
					text = <"*Patient requirements(en)">
					description = <"*Language, transport or other personal requirements to support the patient's attendance or participation in provision of the service.(en)">
				>
				["at0121"] = <
					text = <"*Service requested(en)">
					description = <"*Identification of the service requested. This is often coded with an external terminology.(en)">
				>
				["at0127"] = <
					text = <"*Request status(en)">
					description = <"*The status of the request for service as indicated by the requester. 
Status is used to denote whether this is the initial request, or a follow-up request to change or provide supplementary information.(en)">
				>
				["at0128"] = <
					text = <"*Distribution list for response(en)">
					description = <"*A list of people or organisation who should receive copies of any communication.(en)">
				>
				["at0132"] = <
					text = <"*Specific details(en)">
					description = <"*Structured detail defining the service requested eg CLUSTER archetype specifying the specific catheter that is to be inserted.(en)">
				>
				["at0135"] = <
					text = <"*Description of service(en)">
					description = <"*A detailed narrative description of the service requested.(en)">
				>
				["at0136"] = <
					text = <"*Emergency(en)">
					description = <"*The request is an emergency.(en)">
				>
				["at0137"] = <
					text = <"*Urgent(en)">
					description = <"*The request is urgent.(en)">
				>
				["at0138"] = <
					text = <"*Routine(en)">
					description = <"*The request is routine.(en)">
				>
				["at0141"] = <
					text = <"*Requestor(en)">
					description = <"*Details about the healthcare provider or organisation requesting the service.(en)">
				>
				["at0142"] = <
					text = <"*Receiver(en)">
					description = <"*Details about the healthcare provider or organisation receiving the request for service.(en)">
				>
				["at0144"] = <
					text = <"*Latest date service required(en)">
					description = <"*The latest date that is acceptable for the service to be completed.(en)">
				>
			>
		>
		["en"] = <
			items = <
				["at0.2"] = <
					text = <"Duration">
					description = <"Length of time the referral is valid.">
				>
				["at0.3"] = <
					text = <"Duration">
					description = <"Duration for which the referral is valid.">
				>
				["at0.4"] = <
					text = <"Indefinite">
					description = <"If true, referral is for an indefinite period of time.">
				>
				["at0.5"] = <
					text = <"Contacts">
					description = <"A list of people or organisations who are relevant to this referral, including next of kin.">
				>
				["at0000"] = <
					text = <"Healthcare service request">
					description = <"Generic request for a range of different healthcare services e.g referral, lab request, equipment request.">
				>
				["at0000.1"] = <
					text = <"Referral request">
					description = <"Request for provision of a specified service by another healthcare provider or organisation.">
				>
				["at0001"] = <
					text = <"Request">
					description = <"Current Activity.">
				>
				["at0008"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0009"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0010"] = <
					text = <"Requestor Identifier">
					description = <"The local ID assigned to the order by the healthcare provider or organisation requesting the service. This is also referred to as Placer Order Identifier.">
				>
				["at0011"] = <
					text = <"Receiver identifier">
					description = <"The ID assigned to the order by the healthcare provider or organisation receiving the request for service. This is also referred to as Filler Order Identifier.">
				>
				["at0040"] = <
					text = <"Date &/or time service required">
					description = <"The date and time that the service should be performed or completed.">
				>
				["at0062"] = <
					text = <"Reason for request">
					description = <"A short description of the reason for the request.  This is often coded with an external terminology.">
				>
				["at0064"] = <
					text = <"Reason description">
					description = <"A narrative description explaining the reason for request.">
				>
				["at0065"] = <
					text = <"Intent">
					description = <"Stated intent of the request by the referrer.">
				>
				["at0068"] = <
					text = <"Urgency">
					description = <"Urgency of the request.">
				>
				["at0076"] = <
					text = <"Supplementary information to follow">
					description = <"True indicates that additional information has been identified and will be forwarded when available eg incomplete pathology test results.">
				>
				["at0078"] = <
					text = <"Supplementary information expected">
					description = <"Details of the nature of supplementary information that is to follow e.g name of laboratory results.">
				>
				["at0112"] = <
					text = <"Localisation">
					description = <"Local detailed information such as billing requirements.">
				>
				["at0116"] = <
					text = <"Patient requirements">
					description = <"Language, transport or other personal requirements to support the patient's attendance or participation in provision of the service.">
				>
				["at0121"] = <
					text = <"Service requested">
					description = <"Identification of the service requested. This is often coded with an external terminology.">
				>
				["at0127"] = <
					text = <"Request status">
					description = <"The status of the request for service as indicated by the requester. 
Status is used to denote whether this is the initial request, or a follow-up request to change or provide supplementary information.">
				>
				["at0128"] = <
					text = <"Distribution list for response">
					description = <"A list of people or organisation who should receive copies of any communication.">
				>
				["at0132"] = <
					text = <"Specific details">
					description = <"Structured detail defining the service requested eg CLUSTER archetype specifying the specific catheter that is to be inserted.">
				>
				["at0135"] = <
					text = <"Description of service">
					description = <"A detailed narrative description of the service requested.">
				>
				["at0136"] = <
					text = <"Emergency">
					description = <"The request is an emergency.">
				>
				["at0137"] = <
					text = <"Urgent">
					description = <"The request is urgent.">
				>
				["at0138"] = <
					text = <"Routine">
					description = <"The request is routine.">
				>
				["at0141"] = <
					text = <"Requestor">
					description = <"Details about the healthcare provider or organisation requesting the service.">
				>
				["at0142"] = <
					text = <"Receiver">
					description = <"Details about the healthcare provider or organisation receiving the request for service.">
				>
				["at0144"] = <
					text = <"Latest date service required">
					description = <"The latest date that is acceptable for the service to be completed.">
				>
			>
		>
	>
