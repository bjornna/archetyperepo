archetype (adl_version=1.4)
	openEHR-EHR-OBSERVATION.marietal_status.v1

concept
	[at0000]	-- *Sivilstatus(nb)
language
	original_language = <[ISO_639-1::nb]>
	translations = <
		["no"] = <
			language = <[ISO_639-1::no]>
			author = <
				["name"] = <"?">
			>
		>
		["en"] = <
			language = <[ISO_639-1::en]>
			author = <
				["name"] = <"?">
			>
		>
	>
description
	original_author = <
		["name"] = <"">
	>
	details = <
		["no"] = <
			language = <[ISO_639-1::no]>
			purpose = <"*Benyttes til å dokumentere sivilstatus til en pasient i forbindelse med kartlegging av familie-/sosialanamnese(nb)">
			use = <"*Benyttes til å dokumentere sivilstatus til en pasient.(nb)">
			misuse = <"*Skal ikke benyttes til å dokumentere sivilstatus til andre enn pasienten, feks foreldre. (nb)">
			copyright = <"*(nb)">
		>
		["en"] = <
			language = <[ISO_639-1::en]>
			purpose = <"Use to document the patients marietal status as part of a social status recording. ">
			use = <"Use to document the patients marietal status">
			misuse = <"Not for documenting marietal status for others than the patient, ie do not use for the patients parents etc.">
			copyright = <"">
		>
		["nb"] = <
			language = <[ISO_639-1::nb]>
			purpose = <"Benyttes til å dokumentere sivilstatus til en pasient i forbindelse med kartlegging av familie-/sosialanamnese">
			use = <"Benyttes til å dokumentere sivilstatus til en pasient.">
			misuse = <"Skal ikke benyttes til å dokumentere sivilstatus til andre enn pasienten, feks foreldre. ">
			copyright = <"">
		>
	>
	lifecycle_state = <"AuthorDraft">
	other_contributors = <>
	other_details = <
		["MD5-CAM-1.0.1"] = <"2E4664001C0EF25EE4A8BA23FFD5ACD8">
	>

definition
	OBSERVATION[at0000] matches {	-- *Sivilstatus(nb)
		data matches {
			HISTORY[at0001] matches {	-- *Event Series(nb)
				events cardinality matches {1..*; unordered} matches {
					EVENT[at0002] occurrences matches {0..1} matches {	-- *Any event(nb)
						data matches {
							ITEM_TREE[at0003] matches {	-- *Tree(nb)
								items cardinality matches {0..*; unordered} matches {
									ELEMENT[at0004] occurrences matches {0..1} matches {	-- *Sivilstatus(nb)
										value matches {
											DV_CODED_TEXT matches {
												defining_code matches {
													[local::
													at0005, 	-- *Gift(nb)
													at0006, 	-- *Samboer(nb)
													at0007, 	-- *Skilt(nb)
													at0008, 	-- *Enke(mann)(nb)
													at0009, 	-- *Ugift/enslig(nb)
													at0010]	-- *Annet(nb)
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

ontology
	term_definitions = <
		["no"] = <
			items = <
				["at0000"] = <
					text = <"*Sivilstatus(nb)">
					description = <"*Benyttes til å dokumentere sivilstatus(nb)">
				>
				["at0001"] = <
					text = <"*Event Series(nb)">
					description = <"*@ internal @(nb)">
				>
				["at0002"] = <
					text = <"*Any event(nb)">
					description = <"**(nb)">
				>
				["at0003"] = <
					text = <"*Tree(nb)">
					description = <"*@ internal @(nb)">
				>
				["at0004"] = <
					text = <"*Sivilstatus(nb)">
					description = <"**(nb)">
				>
				["at0005"] = <
					text = <"*Gift(nb)">
					description = <"**(nb)">
				>
				["at0006"] = <
					text = <"*Samboer(nb)">
					description = <"**(nb)">
				>
				["at0007"] = <
					text = <"*Skilt(nb)">
					description = <"**(nb)">
				>
				["at0008"] = <
					text = <"*Enke(mann)(nb)">
					description = <"**(nb)">
				>
				["at0009"] = <
					text = <"*Ugift/enslig(nb)">
					description = <"**(nb)">
				>
				["at0010"] = <
					text = <"*Annet(nb)">
					description = <"**(nb)">
				>
			>
		>
		["en"] = <
			items = <
				["at0000"] = <
					text = <"Marietal status">
					description = <"unknown">
				>
				["at0001"] = <
					text = <"Event Series">
					description = <"@ internal @">
				>
				["at0002"] = <
					text = <"Any event">
					description = <"*">
				>
				["at0003"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0004"] = <
					text = <"Marietal status">
					description = <"*">
				>
				["at0005"] = <
					text = <"Married">
					description = <"*">
				>
				["at0006"] = <
					text = <"Cohabitant">
					description = <"*">
				>
				["at0007"] = <
					text = <"Divorced">
					description = <"*">
				>
				["at0008"] = <
					text = <"Widow(er)">
					description = <"*">
				>
				["at0009"] = <
					text = <"Unmarried/Single">
					description = <"*">
				>
				["at0010"] = <
					text = <"Other">
					description = <"*">
				>
			>
		>
		["nb"] = <
			items = <
				["at0000"] = <
					text = <"Sivilstatus">
					description = <"Benyttes til å dokumentere sivilstatus">
				>
				["at0001"] = <
					text = <"Event Series">
					description = <"@ internal @">
				>
				["at0002"] = <
					text = <"Any event">
					description = <"*">
				>
				["at0003"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0004"] = <
					text = <"Sivilstatus">
					description = <"*">
				>
				["at0005"] = <
					text = <"Gift">
					description = <"*">
				>
				["at0006"] = <
					text = <"Samboer">
					description = <"*">
				>
				["at0007"] = <
					text = <"Skilt">
					description = <"*">
				>
				["at0008"] = <
					text = <"Enke(mann)">
					description = <"*">
				>
				["at0009"] = <
					text = <"Ugift/enslig">
					description = <"*">
				>
				["at0010"] = <
					text = <"Annet">
					description = <"*">
				>
			>
		>
	>
