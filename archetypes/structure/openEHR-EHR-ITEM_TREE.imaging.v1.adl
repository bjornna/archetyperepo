archetype (adl_version=1.4)
    openEHR-EHR-ITEM_TREE.imaging.v1

concept
    [at0000]

language
    original_language = <[ISO_639-1::en]>
    translations = <
        ["nb"] = <
            language = <[ISO_639-1::nb]>
            author = <
                ["name"] = <"?">
            >
        >
        ["no"] = <
            language = <[ISO_639-1::no]>
            author = <
                ["name"] = <"?">
            >
        >
    >

description
    original_author = <
        ["email"] = <"sam.heard@oceaninformatics.com">
        ["organisation"] = <"Ocean Informatics">
        ["name"] = <"Sam Heard">
        ["date"] = <"9/01/2007">
    >
    lifecycle_state = <"AuthorDraft">
    details = <
        ["en"] = <
            language = <[ISO_639-1::en]>
            purpose = <"Data for the description of the action or instruction related to imaging">
            keywords = <"Xray","scan","MRI","CT","nuclear","ultrasound">
            copyright = <"© openEHR Foundation">
        >
        ["nb"] = <
            language = <[ISO_639-1::nb]>
            purpose = <"Data for the description of the action or instruction related to imaging">
            keywords = <"*Xray(en)","*scan(en)","*MRI(en)","*CT(en)","*nuclear(en)","*ultrasound(en)">
            copyright = <"*© openEHR Foundation(en)">
            use = <"*(en)">
            misuse = <"*(en)">
        >
        ["no"] = <
            language = <[ISO_639-1::no]>
            purpose = <"Data for the description of the action or instruction related to imaging">
            keywords = <"*Xray(en)","*scan(en)","*MRI(en)","*CT(en)","*nuclear(en)","*ultrasound(en)">
            copyright = <"*© openEHR Foundation(en)">
            use = <"*(en)">
            misuse = <"*(en)">
        >
    >

definition
    ITEM_TREE[at0001] occurrences matches {0..*} matches {
        items cardinality matches {1..*; ordered} matches {
            CLUSTER[at0002] occurrences matches {0..1} matches {
                items cardinality matches {1..1; unordered} matches {
                    ELEMENT[at0003] matches {
                        value matches {
                            DV_TEXT matches {*}
                        }
                    }
                }
            }
            CLUSTER[at0004] occurrences matches {1..*} matches {
                items cardinality matches {1..*; unordered} matches {
                    ELEMENT[at0005] matches {
                        value matches {
                            DV_TEXT matches {*}
                            DV_CODED_TEXT matches {
                                defining_code matches {
                                    [ac0001]
                                }
                            }
                        }
                    }
                    ELEMENT[at0007] occurrences matches {0..1} matches {
                        value matches {
                            DV_TEXT matches {*}
                        }
                    }
                    ELEMENT[at0006] occurrences matches {0..*} matches {
                        value matches {
                            DV_TEXT matches {*}
                        }
                    }
                }
            }
            CLUSTER[at0008] occurrences matches {0..1} matches {
                items cardinality matches {1..*; unordered} matches {
                    ELEMENT[at0009] occurrences matches {0..1} matches {
                        value matches {
                            DV_DATE_TIME matches {*}
                        }
                    }
                    ELEMENT[at0010] occurrences matches {0..1} matches {
                        value matches {
                            DV_TEXT matches {*}
                        }
                    }
                }
            }
        }
    }

ontology
    term_definitions = <
        ["no"] = <
            items = <
                ["at0000"] = <
                    text = <"Bilde data">
                    description = <"Data registrert om en bildeundersøkelse eller instruksjon">

                >
                ["at0001"] = <
                    text = <"Tree">
                    description = <"@ internal @">

                >
                ["at0002"] = <
                    text = <"Klinisk informasjon">
                    description = <"Clinical information relevant to the imaging investigation">

                >
                ["at0003"] = <
                    text = <"Funn">
                    description = <"Clinical findings relevant to the imaging investigation">

                >
                ["at0004"] = <
                    text = <"Bilde">
                    description = <"Information about the imaging">

                >
                ["at0005"] = <
                    text = <"Bilde prosedyre">
                    description = <"Bildetype ">

                >
                ["at0006"] = <
                    text = <"View">
                    description = <"The imaging views">

                >
                ["at0007"] = <
                    text = <"Anatomisk sted">
                    description = <"The anatomical site (or object) for imaging">

                >
                ["at0008"] = <
                    text = <"Prosess">
                    description = <"The process of the imaging investigation">

                >
                ["at0009"] = <
                    text = <"Bildedato">
                    description = <"The date the imaging is to be or was carried out.">

                >
                ["at0010"] = <
                    text = <"Lokasjon">
                    description = <"The location where the imaging is to be or was carried out">

                >
            >
        >
        ["en"] = <
            items = <
                ["at0000"] = <
                    text = <"Imaging data">
                    description = <"Data recorded about an imaging action or instruction">

                >
                ["at0001"] = <
                    text = <"Tree">
                    description = <"@ internal @">

                >
                ["at0002"] = <
                    text = <"Clinical information">
                    description = <"Clinical information relevant to the imaging investigation">

                >
                ["at0003"] = <
                    text = <"Findings">
                    description = <"Clinical findings relevant to the imaging investigation">

                >
                ["at0004"] = <
                    text = <"Imaging">
                    description = <"Information about the imaging">

                >
                ["at0005"] = <
                    text = <"Imaging procedure">
                    description = <"The type of imaging">

                >
                ["at0006"] = <
                    text = <"Views">
                    description = <"The imaging views">

                >
                ["at0007"] = <
                    text = <"Anatomical site">
                    description = <"The anatomical site (or object) for imaging">

                >
                ["at0008"] = <
                    text = <"Process">
                    description = <"The process of the imaging investigation">

                >
                ["at0009"] = <
                    text = <"Date of imaging">
                    description = <"The date the imaging is to be or was carried out.">

                >
                ["at0010"] = <
                    text = <"Location">
                    description = <"The location where the imaging is to be or was carried out">

                >
            >
        >
        ["nb"] = <
            items = <
                ["at0000"] = <
                    text = <"Bilde data">
                    description = <"Data registrert om en bildeundersøkelse eller instruksjon">

                >
                ["at0001"] = <
                    text = <"Tree">
                    description = <"@ internal @">

                >
                ["at0002"] = <
                    text = <"Klinisk informasjon">
                    description = <"Clinical information relevant to the imaging investigation">

                >
                ["at0003"] = <
                    text = <"Funn">
                    description = <"Clinical findings relevant to the imaging investigation">

                >
                ["at0004"] = <
                    text = <"Bilde">
                    description = <"Information about the imaging">

                >
                ["at0005"] = <
                    text = <"Bilde prosedyre">
                    description = <"Bildetype ">

                >
                ["at0006"] = <
                    text = <"View">
                    description = <"The imaging views">

                >
                ["at0007"] = <
                    text = <"Anatomisk sted">
                    description = <"The anatomical site (or object) for imaging">

                >
                ["at0008"] = <
                    text = <"Prosess">
                    description = <"The process of the imaging investigation">

                >
                ["at0009"] = <
                    text = <"Bildedato">
                    description = <"The date the imaging is to be or was carried out.">

                >
                ["at0010"] = <
                    text = <"Lokasjon">
                    description = <"The location where the imaging is to be or was carried out">

                >
            >
        >
    >
    constraint_definitions = <
        ["no"] = <
            items = <
                ["ac0001"] = <
                    text = <"*New constraint(en)">
                    description = <"**(en)">

                >
            >
        >
        ["en"] = <
            items = <
                ["ac0001"] = <
                    text = <"New constraint">
                    description = <"*">

                >
            >
        >
        ["nb"] = <
            items = <
                ["ac0001"] = <
                    text = <"*New constraint(en)">
                    description = <"**(en)">

                >
            >
        >
    >
    term_binding = <
    >
    constraint_binding = <
    >
